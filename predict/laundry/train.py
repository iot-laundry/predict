from model import LaundryModel, LaundryUtil
import matplotlib.pyplot as plt

laundryModel = LaundryModel()

# Make the generators for each directory
train_generator = LaundryUtil.make_generator('./images/train')
validation_generator = LaundryUtil.make_generator('./images/validation')

# Train the model
history = laundryModel.train_model(train_generator, validation_generator)
LaundryUtil.plot_history(history)
