# Prediction Service

Parts of the service
* Predict package - flask app that wraps the laundry package for runtime predictions
* Laundry package - tensorflow library for training and runtime

![Training Result](./training.png)